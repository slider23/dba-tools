#!/usr/bin/env bash

cd "${BASE_DIR}" || exit

## Import required config / libraries
## Local includes
source config/prepare_backup.sh
source config/base_config.sh

# Util library includes.
source lib/logger.sh

DBDIST=$( mysql --version | grep MariaDB )
if [[ -n "${DBDIST}" ]]; then
    logger "Detected MariaDB ..."
    BACKUPBIN=mariabackup
    RESTORE_OPTS=( "--prepare" "--apply-log-only" )
else
    logger "Detected MySQL or MariaDB < 10.x ..."
    BACKUPBIN=xtrabackup
    RESTORE_OPTS=( "--prepare" "--apply-log-only" )
fi

# Get the full + incremental dirs
shopt -s nullglob
FULL_DIRS=( ${RESTOREDIR}/full-*/ )
INCR_DIRS=( ${RESTOREDIR}/incremental-*/ )
shopt -u nullglob

# Set our full backup
FULL_BACKUP_DIR="${FULL_DIRS[0]}"

# Do we even have a full backup to prepare?
# Or maybe too many.
if [[ ${#FULL_DIRS[@]} != 1 ]]; then
    logger "Full backup directory not found. Did you run extract-backups.sh?" 
    exit 1
fi

logger "Initial prepare of full backup ..."

"${BACKUPBIN}" "${RESTORE_OPTS}" --target-dir="${FULL_BACKUP_DIR}" 2> "${LOG_DIR}/prepare-progress.log"

# Loop through incremental directories, if any, apply them to the full backup
for INCR in "${INCR_DIRS[@]}"
do
    logger "Applying ${INCR} to ${FULL_BACKUP_DIR} ..."
    "${BACKUPBIN}" "${RESTORE_OPTS}" --incremental-dir="${INCR}" --target-dir="${FULL_BACKUP_DIR}" 2>> "${LOG_DIR}/prepare-progress.log"
done

logger "Doing final prepare to full backup."

"${BACKUPBIN}" "${RESTORE_OPTS}" --target-dir="${FULL_BACKUP_DIR}" 2>> "${LOG_DIR}/prepare-progress.log"

# "completed OK" appears as (Full + IncrCount) + 1
OK_COUNT="$(grep -c 'completed OK' "${LOG_DIR}/prepare-progress.log")"

# Mariabackup, for one reason or another, generates 1 additional "completed OK!"
# We add one to systems that use xtrabackup in order to pass the check below.
if [[ "${BACKUPBIN}" = "xtrabackup" ]]; then
    OK_COUNT=$((OK_COUNT + 1))
fi

# Check our "completed OK!" count. Should be: #Full + #Incr + 1
if (( ${OK_COUNT} == (${#FULL_DIRS[@]} + ${#INCR_DIRS[@]} + 1) )); then
    echo ""
    logger "The backup appears to be fully prepared."
    echo "Please check the ${LOG_DIR}/prepare-progress.log file to verify before continuing."
    echo ""
    echo "You will find the ready-to-restore backup in ${FULL_DIRS[0]}"
    echo ""
    echo "To restore this, you will need to do the following:"
    echo "  * Stop MySQL"
    echo "  * Clean out your datadir ( Default: /var/lib/mysql )"
    echo "  * ${BACKUPBIN} --copy-back --target-dir=${FULL_DIRS[0]}"
    echo ""
    echo "After that, ensure the mysql user owns all the files in the datadir and start mysql."
    echo ""
else
    echo ""
    echo "Prepare failed. Check ${LOG_DIR}/prepare-progress.log"
fi