#!/usr/bin/env bash

## Import required config / libraries
## Local includes
source config/max_connection_watcher.sh
source config/base_config.sh

# Util library includes.
source lib/logger.sh

MAX_CONN=$( mysql -sN "${DB_OPTS[@]}" -e "SHOW VARIABLES LIKE 'max_connections'" 2>&1 | grep -v "Using a password" | awk '{print $2}' )
THREADS_CONNECTED=$( mysql -sN "${DB_OPTS[@]}" -e "SHOW STATUS LIKE 'Threads_connected'" 2>&1 | grep -v "Using a password" | awk '{print $2}' )

DESIRED_MAX=$( echo "${MAX_CONN} * ${MAX_CONN_ALLOWED_PCT}" | bc | awk '{print int($1+0.5)}' )

if [[ ${RES} -ge ${DESIRED_MAX} ]]; then
    logger "max_connections > ${DESIRED_MAX}"
fi