#!/usr/bin/env bash

# This script is used to monitor replication thread/sql state. Not lag.

## Import required info / config / libraries
## Local includes
source config/replication_state_monitor.sh
source config/base_config.sh

# Util library includes.
source lib/logger.sh

cd "${BASE_DIR}" || exit

DB_OPTS=( "-u${DB_USER}" )

if [[ -n "${DB_PASS}" ]]; then
    DB_OPTS+=( "-p${DB_PASS}" )
fi

DBDIST=$( mysql --version | grep MariaDB )

if [[ -n "${DBDIST}" ]]; then
    DIST="mariadb"
    STATUS="SHOW ALL SLAVES STATUS"
    SEARCH_NAME="Connection_name"
else
    DIST="mysql"
    STATUS="SHOW SLAVE STATUS"
    SEARCH_NAME="Channel_Name"
fi

logger "Beginning replication state check ..."

SLAVES=( $( mysql "${DB_OPTS[@]}" -e "${STATUS}\G" 2>&1 | grep -v "Using a password" | grep "${SEARCH_NAME}" | awk '{print $2}' ) )

# Not multi-source
if [[ -z "${SLAVES}" ]]; then
    logger "Single replication instance found ..."
    SLAVE_IO=$( mysql "${DB_OPTS[@]}" -e "SHOW SLAVE STATUS\G" 2>&1 | grep -v "Using a password" | grep Slave_IO_Running | awk '{print $2}' )
    SLAVE_SQL=$( mysql "${DB_OPTS[@]}" -e "SHOW SLAVE STATUS\G" 2>&1 | grep -v "Using a password" | grep Slave_SQL_Running | grep -v State | awk '{print $2}' )

    if [[ "${SLAVE_IO}" != "Yes" ]] || [[ "${SLAVE_SQL}" != "Yes" ]]; then
        logger "Replication is broken!"
        exit 0
    fi
else
    # Multisource
    for x in "${SLAVES[@]}"
    do
        logger "Checking replication on channel '${x}' ..."
    
        case "${DIST}" in
            "mariadb" )
                SLAVE_IO=$( mysql "${DB_OPTS[@]}" -e "SHOW SLAVE '${x}' STATUS\G" 2>&1 | grep -v "Using a password" | grep Slave_IO_Running | awk '{print $2}' )
                SLAVE_SQL=$( mysql "${DB_OPTS[@]}" -e "SHOW SLAVE '${x}' STATUS\G" 2>&1 | grep -v "Using a password" | grep Slave_SQL_Running | grep -v State | awk '{print $2}' )
            ;;
            "mysql" )
                SLAVE_IO=$( mysql "${DB_OPTS[@]}" -e "SHOW SLAVE STATUS FOR CHANNEL '${x}'\G" 2>&1 | grep -v "Using a password" | grep Slave_IO_Running | awk '{print $2}' )
                SLAVE_SQL=$( mysql "${DB_OPTS[@]}" -e "SHOW SLAVE STATUS FOR CHANNEL '${x}'\G" 2>&1 | grep -v "Using a password" | grep Slave_SQL_Running | grep -v State | awk '{print $2}' )
            ;;
            * )
                echo "DIST not set?"
                exit 1
            ;;
        esac

        if [[ "${SLAVE_IO}" != "Yes" ]] || [[ ${SLAVE_SQL} != "Yes" ]]; then
            logger "Replication is broken on ${x}!"
            exit 0
        else
            logger "Replication check passed on '${x}' ..."
        fi

    done
fi