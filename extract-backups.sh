#!/usr/bin/env bash

cd "${BASE_DIR}" || exit

## Import required config / libraries
## Local includes
source config/extract_backups.sh
source config/base_config.sh

# Util library includes.
source lib/logger.sh

BACKUP_TO_RESTORE="${BACKUPDIR}/${1}"

DBDIST=$( mysql --version | grep MariaDB )

if [[ -n "${DBDIST}" ]]; then
    logger "Selected mariabackup as the BACKUPBIN ..."
    BACKUPBIN=mariabackup
    STREAMBIN=mbstream
else
    logger "Selected xtrabackup as the BACKUPBIN ..."
    BACKUPBIN=xtrabackup
    STREAMBIN=xbstream
fi

if [[ -z $( command -v qpress ) ]]; then
    logger "qpress not found. Please install the qpress package."
    exit 1
fi

if [[ "${1}" = "" ]] || [[ ! -d "${BACKUPDIR}/${1}" ]]; then
    echo "Directory not specified or does not exist."
    echo ""
    echo "Usage: extract-backups.sh [backup_(date)]"
    echo ""
    echo "Available backup directories: "
    for x in $(ls "${BACKUPDIR}" )
    do
        if [[ "${x}" != "restore" ]]; then
            echo -e "    ${x}"
        fi
    done
    echo ""
    exit 1
fi

logger "Starting extract script ..."

DB_OPTS=( "-u${DB_USER}" )

if [[ -n "${DB_PASS}" ]]; then
    DB_OPTS+=( "-p${DB_PASS}" )
fi

cd "${BACKUP_TO_RESTORE}"

BACKUP_OPTS=( "--decompress" )

if [[ ${PARALLEL} -eq 1 ]]; then
    logger "Parallel compression is enabled in the config ..."
    # Make sure we have nproc, even though there are other ways.
    if [[ -n $( command -v nproc ) ]]; then
        NPROC=$( echo "$( nproc --all ) / 2" | bc | awk '{print int($1+0.5)}' )
        logger "Setting --parallel to ${NPROC} ( Total cores / 2, rounded up )"
        BACKUP_OPTS+=(
            "--parallel=${NPROC}"
        )
    else
        logger "nproc command not found, parallel decompression will be disabled ..."
    fi
fi

FILES=( $( ls *.xbstream ) )

for FILE in "${FILES[@]}"
do
    BASE_FILENAME="$( basename "${FILE%.xbstream}" )"
    RESTORE_DIR="${BACKUPDIR}/restore/${BASE_FILENAME}"
    mkdir -p "${RESTORE_DIR}"
    logger "Extracting ${FILE} ..."
    "${STREAMBIN}" -x -C "${RESTORE_DIR}" < "${FILE}"

    # "Bug" in MariaBackup, caused by --extra-lsndir
    if [[ -f "${BACKUP_TO_RESTORE}/xtrabackup_info.qp" ]]; then
        rm -f "${BACKUP_TO_RESTORE}/xtrabackup_info.qp"
    fi

    logger "Decompressing contents of ${FILE} ..."

    echo "" >> "${LOG_DIR}/extract-${DATE}.log"
    echo "----- ${FILE} -----" >> "${LOG_DIR}/extract-${DATE}.log"
    echo "" >> "${LOG_DIR}/extract-${DATE}.log"

    "${BACKUPBIN}" "${BACKUP_OPTS[@]}" --target-dir="${RESTORE_DIR}" 2> "${LOG_DIR}/extract-${FILE}-${DATE}.log"
    find "${RESTORE_DIR}" -name '*.qp' -delete
done

logger "Extraction complete."